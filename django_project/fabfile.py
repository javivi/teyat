# -*- coding: utf-8 -*-
from fabric.api import local, lcd

def deploy():
    with lcd('/home/django/prod/teyat/'):
        local('git pull /home/django/dev/teyat/')
    with lcd('/home/django/prod/teyat/django_project/'):
        local('python manage.py migrate')
        local('python manage.py collectstatic --noinput')
        #local('python manage.py test wildcaribe')
    with lcd('/home/django/prod/teyat/django_project/teyat/'):        
        local('python ../manage.py compilemessages')
        local('echo DON´T FORGET TO "sudo service gunicorn restart"')
        #local('exit')
        #local('service gunicorn restart')
        #local('su django')

