# -*- coding: utf-8 -*-
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class TeyatConfig(AppConfig):
    name = 'teyat'
    verbose_name = _(u'Teyat: Gestión del Teatro')
    label = 'teyat'