# -*- coding: utf-8 -*-
from __future__ import absolute_import

import rules

def model_field_exists(clf, field):
    return hasattr(clf, field)

# Predicates
@rules.predicate
def is_user_owner_of_object(user, object):
    if not object:
        return True
    if model_field_exists(object,'owner'):
        if user.username in object.owner.users.values_list('username', flat=True):
            return True
    elif model_field_exists(object,'users'):
        if user.username in object.users.values_list('username', flat=True):
            return True
    elif model_field_exists(object, 'campanya'):
        if user.username in object.campanya.owner.users.values_list('username', flat=True):
            return True
    elif model_field_exists(object, 'obra_campanya'):
        if user.username in object.obra_campanya.campanya.owner.users.values_list('username', flat=True):
            return True
    elif model_field_exists(object, 'centro'):
        if user.username in object.centro.owner.users.values_list('username', flat=True):
            return True
    #elif model_field_exists(object, 'adjunto'):
    #    if user.username in object.adjunto.owner.users.values_list('username', flat=True):
    #        return True
    elif model_field_exists(object, 'correo'):
        if user.username in object.correo.owner.users.values_list('username', flat=True):
            return True
    
    return False
        
@rules.predicate
def is_not_user_test(user, object):
    if not object:
        return True
    if user.username=='test':
        return False
    else:
        return True
'''
@rules.predicate
def is_user_owner_of_funcion(user, funcion):
    if not funcion:
        return True
    if user.username in funcion.obra_campanya.obra.owner.users.values_list('username', flat=True):
        return True
    else:
        return False
'''

        
# Permissions

rules.add_perm('teyat', rules.always_allow)

rules.add_perm('teyat.add_centro', rules.is_staff)
rules.add_perm('teyat.change_centro', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_centro', is_user_owner_of_object) 

rules.add_perm('teyat.add_persona_contacto', rules.is_staff)
rules.add_perm('teyat.change_persona_contacto', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_persona_contacto', is_user_owner_of_object) 

rules.add_perm('teyat.add_companyia', rules.is_staff)
rules.add_perm('teyat.change_companyia', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_companyia', is_user_owner_of_object) 

rules.add_perm('teyat.add_obra', rules.is_staff)
rules.add_perm('teyat.change_obra', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_obra', is_user_owner_of_object) 

rules.add_perm('teyat.add_campanya', rules.is_staff)
rules.add_perm('teyat.change_campanya', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_campanya', is_user_owner_of_object) 

rules.add_perm('teyat.add_obraencampanya', rules.is_staff)
rules.add_perm('teyat.change_obraencampanya', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_obraencampanya', is_user_owner_of_object) 

rules.add_perm('teyat.add_funcion', rules.is_staff)
rules.add_perm('teyat.change_funcion', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_funcion', is_user_owner_of_object) 

#rules.add_perm('teyat.add_aforo_funcion', rules.is_staff)
rules.add_perm('teyat.change_aforo_funcion', is_user_owner_of_object | rules.is_superuser)
#rules.add_perm('teyat.delete_aforo_funcion', is_user_owner_of_object) 

#rules.add_perm('teyat.add_recaudacion_funcion', rules.is_staff)
rules.add_perm('teyat.change_recaudacion_funcion', is_user_owner_of_object | rules.is_superuser)
#rules.add_perm('teyat.delete_recaudacion_funcion', is_user_owner_of_object) 

#rules.add_perm('teyat.add_aforo_campanya', rules.is_staff)
rules.add_perm('teyat.change_aforo_campanya', is_user_owner_of_object | rules.is_superuser)
#rules.add_perm('teyat.delete_aforo_campanya', is_user_owner_of_object) 

#rules.add_perm('teyat.add_recaudacion_campanya', rules.is_staff)
rules.add_perm('teyat.change_recaudacion_campanya', is_user_owner_of_object | rules.is_superuser)
#rules.add_perm('teyat.delete_recaudacion_campanya', is_user_owner_of_object) 

#rules.add_perm('teyat.add_aforo_obraencampanya', rules.is_staff)
rules.add_perm('teyat.change_aforo_obraencampanya', is_user_owner_of_object | rules.is_superuser)
#rules.add_perm('teyat.delete_aforo_obraencampanya', is_user_owner_of_object) 

#rules.add_perm('teyat.add_recaudacion_obraencampanya', rules.is_staff)
rules.add_perm('teyat.change_recaudacion_obraencampanya', is_user_owner_of_object | rules.is_superuser)
#rules.add_perm('teyat.delete_recaudacion_obraencampanya', is_user_owner_of_object) 


rules.add_perm('teyat.add_reserva', rules.is_staff)
rules.add_perm('teyat.change_reserva', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_reserva', is_user_owner_of_object) 

rules.add_perm('teyat.add_comarca', rules.is_staff)
rules.add_perm('teyat.change_comarca', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_comarca', is_user_owner_of_object)

rules.add_perm('teyat.add_poblacion', rules.is_staff)
rules.add_perm('teyat.change_poblacion', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_poblacion', is_user_owner_of_object)

rules.add_perm('teyat.add_idioma', rules.is_staff)
rules.add_perm('teyat.change_idioma', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_idioma', is_user_owner_of_object)

rules.add_perm('teyat.add_correoconfirmacionreserva', rules.is_staff)
rules.add_perm('teyat.change_correoconfirmacionreserva', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_correoconfirmacionreserva', is_user_owner_of_object)

rules.add_perm('teyat.add_correoreserva', rules.is_staff)
rules.add_perm('teyat.change_correoreserva', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_correoreserva', is_user_owner_of_object)

rules.add_perm('teyat.add_adjunto', rules.is_staff)
rules.add_perm('teyat.change_adjunto', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_adjunto', is_user_owner_of_object)

rules.add_perm('teyat.add_adjuntocorreo', rules.is_staff)
rules.add_perm('teyat.change_adjuntocorreo', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_adjuntocorreo', is_user_owner_of_object)

#rules.add_perm('teyat.add_logcorreosrecordatorios', rules.is_staff)
rules.add_perm('teyat.change_logcorreosrecordatorios', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_logcorreosrecordatorios', is_user_owner_of_object)

#rules.add_perm('teyat.add_logcorreosestadoreserva', rules.is_staff)
rules.add_perm('teyat.change_logcorreosestadoreserva', is_user_owner_of_object | rules.is_superuser)
rules.add_perm('teyat.delete_logcorreosestadoreserva', is_user_owner_of_object)

rules.add_perm('teyat.change_teatro', is_user_owner_of_object | rules.is_superuser)
