# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions
from teyat.models import Campanya, Obra, Idioma, ObraEnCampanya, Reserva
from teyat.models import Persona_contacto, CorreoConfirmacionReserva, CorreoReserva
from teyat.models import Aforo_Campanya, Recaudacion_Campanya


class Campanya_TO(TranslationOptions):
    fields = ('nombre',)
 
translator.register(Campanya, Campanya_TO)
translator.register(Aforo_Campanya, Campanya_TO)
translator.register(Recaudacion_Campanya, Campanya_TO)

'''class Aforo_Campanya_TO(TranslationOptions):
    fields = ('nombre',)
 
translator.register(Aforo_Campanya, Aforo_Campanya_TO)
'''
'''
class Recaudacion_Campanya_TO(TranslationOptions):
    fields = ('nombre',)
 
translator.register(Recaudacion_Campanya, Recaudacion_Campanya_TO)
'''

'''class Obra_TO(TranslationOptions):
    fields = ('nombre',)
 
translator.register(Obra, Obra_TO)
'''
 
class Idioma_TO(TranslationOptions):
    fields = ('nombre',)
 
translator.register(Idioma, Idioma_TO)

class Persona_contacto_TO(TranslationOptions):
    fields = ('cargo','observaciones',)
 
translator.register(Persona_contacto, Persona_contacto_TO)

class ObraEnCampanya_TO(TranslationOptions):
    fields = ('observaciones',)
translator.register(ObraEnCampanya, ObraEnCampanya_TO)

class Reserva_TO(TranslationOptions):
    fields = ('observaciones',)
translator.register(Reserva, Reserva_TO)


class CorreoConfirmacionReserva_TO(TranslationOptions):
    fields = ('saludo','cuerpo_pre_detalle','cuerpo_post_detalle',)
 
translator.register(CorreoConfirmacionReserva, CorreoConfirmacionReserva_TO)

class CorreoReserva_TO(TranslationOptions):
    fields = ('saludo','cuerpo_pre_detalle','cuerpo_post_detalle',)
 
translator.register(CorreoReserva, CorreoReserva_TO)
