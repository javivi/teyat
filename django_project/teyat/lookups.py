from __future__ import unicode_literals

from selectable.base import ModelLookup
from selectable.registry import registry

from teyat.models import Funcion


class FuncionLookup(ModelLookup):
    model = Funcion
    search_fields = ('name__icontains', )
    
registry.register(FuncionLookup)