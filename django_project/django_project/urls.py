from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from teyat import views
from teyat.admin import ravaladmin, staffadmin, teyatadmin
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.views.decorators.cache import cache_page

#admin.autodiscover() 
#staffadmin.autodiscover()


urlpatterns = patterns('',
    (r'^',include('teyat.urls', namespace='teyat')),
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/', include(teyatadmin.urls)),
    url(r'^raval-admin/', include(ravaladmin.urls)),
    url(r'^staff-admin/', include(staffadmin.urls)),    
    (r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/password_reset/$', auth_views.password_reset, name='admin_password_reset'),
    url(r'^admin/password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url('^markdown/', include( 'django_markdown.urls')),
    url(r'^select2/', include('django_select2.urls')),
)

teyat_lang_patterns = patterns('',
    url(r'^$', cache_page(60 * 30)(views.index), name='index'),
    #url(r'^precios/$', cache_page(60 * 30)(views.prices), name='prices'),
    #url(r'^caracteristicas/$', cache_page(60 * 30)(views.features), name='features'),
    #url(r'^alta/$', cache_page(60 * 30)(views.registro), name='register'),
    #url(r'^contacto/$', cache_page(60 * 60)(views.contacto), name='contacto'),
    #url(r'^sobre-teyat/$', cache_page(60 * 60)(views.about), name='about'),
    #url(r'^gracias-alta/$', views.gracias_alta, name='gracias_alta'),
    #url(r'^nota-legal/$', cache_page(60 * 60)(views.legal), name='legal'),
    #url(r'^cookies/$', cache_page(60 * 60)(views.cookies), name='cookies'),
    
)


urlpatterns += i18n_patterns(' ',
    url(r'^', include(teyat_lang_patterns, namespace='teyat_lang')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', 
              {'document_root': settings.MEDIA_ROOT}),
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', 
              {'document_root': settings.STATIC_ROOT}),   
        ) + static('/', document_root='static/')
